/*
* Chris Vazquez
* 04/19/2016
* Calculate year with highest population given an input array of a random number of individuals born 1900-2000
*/

public void HighestPopYear()
{
    int numAlive = 0;
    int maxAlive = 0;
    int maxYear = 0;
	
	//generate random dataset
    TestCustomer[] cusArray = generateTestCustomers(10000);

    for (int year = 1900; year <= 2000; ++year)
    {
        foreach (TestCustomer customer in cusArray)
        {
            if (customer.dead == false && customer.birthYear == year)
            {
                ++numAlive;
            }
            if (customer.dead == false && customer.deathYear == year)
            {
                --numAlive;
                customer.dead = true;
            }
        }

        if (numAlive > maxAlive)
        {
            maxAlive = numAlive;
            maxYear = year;
        }
    }
}

//Simple method to fill customer array. All individuals have a maximum lifespan of 80 years.
private TestCustomer[] generateTestCustomers(int numberToGenerate)
{
    Random year = new Random();
    List<TestCustomer> customers = new List<TestCustomer>();
    for(int i = 0; i < numberToGenerate; ++i)
    {
        int birthYear = year.Next(1900,2001);

        int deathYear = birthYear + year.Next(0,80);
        customers.Add(new TestCustomer(birthYear, deathYear));
    }

    return customers.ToArray();
}