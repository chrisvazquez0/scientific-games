<?php
/*
* Chris Vazquez
* 04-22-2016
*
* PLEASE NOTE: As of this commit I have about 12 hours total of PHP experience.
* I've spent the past few evenings picking up PHP to complete this task.
*
* Receives a POST request to apply spin results.
* First validates user then updates database and returns updated data as a JSON object.
* Code simply echoes the data for testing purposes.
*/
$mysqli = new mysqli('127.0.0.1', 'root', '','mysql');

if ($mysqli->connect_error) {
    die('Connect Error (' . $mysqli->connect_errno . ') '
            . $mysqli->connect_error);
}

if($_POST) {
	$CusID = htmlspecialchars($_POST["ID"]);
	$Pass = $_POST["Password"];
	$Bet = (int)$_POST["CoinsBet"];
	$Won = (int)$_POST["CoinsWon"];
	
	if(!$mysqli->connect_error) {
		$sql = $mysqli->prepare("SELECT name,credits, lifetime_spins, salt, hash FROM customer WHERE id = ?");
		$sql->bind_param('s', $CusID);
		$sql->execute();
		
		$result = $sql->get_result();
		$row = $result->fetch_assoc();
		
		if ($result != NULL && $result->num_rows == 1) {
			$Name = $row["name"];
			$Salt = $row["salt"];
			$DBHash = $row["hash"];
			$Credits = $row["credits"] - $Bet + $Won;
			$Spins = $row["lifetime_spins"] + 1;
			
			if(hash("sha256",$Salt.$Pass) == $DBHash) {
				$sql = $mysqli->prepare("UPDATE customer SET credits = ?, lifetime_spins = ? WHERE id = ?");
				$sql->bind_param('iis', $Credits, $Spins, $CusID);
				$sql->execute();
				
				$result = $sql->get_result();
				if($mysqli->affected_rows == 1) {
					$AvgReturn = $Credits / $Spins;
					$returnData = array($CusID, $Name, $Credits, $Spins, $AvgReturn);
					
					echo json_encode($returnData);
				}
			} else {
				echo "Invalid password.";
			}
		} elseif ($result != NULL && $result->num_rows > 1) {
			echo "More than one result found. Duplicate customers exist.";
		} else {
			echo "No results found for CusID: $CusID";
		}
	}
}
?>

<form action="SpinRequest.php" method="post"?>
 <p><input type="submit" value="Back to Request"/></p>
</form>