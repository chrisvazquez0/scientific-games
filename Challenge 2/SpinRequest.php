<?php
/*
* Chris Vazquez
* 04-22-2016
*
* PLEASE NOTE: As of this commit I have about 12 hours total of PHP experience.
* I've spent the past few evenings picking up PHP to complete this task.
*
* Simple form for sending spin requests for testing.
* If coming from the CreateUser page it first creates a user, inserting their data into the database.
* Passes Customer's ID, Password, Coins Bet, and Coins Won to SpinResults.php which is where the spin update logic lies.
*/

$mysqli = new mysqli('127.0.0.1', 'root', '','mysql');

if ($mysqli->connect_error) {
    die('Connect Error (' . $mysqli->connect_errno . ') '
            . $mysqli->connect_error);
}

$CusID = "Error";
$Pass = "Error";

if($_POST) {
	if($_POST["FromPage"] == "CreateUser") {
		$CusID = getNextID($mysqli);
		$Name = htmlspecialchars($_POST["Name"]);
		$Pass = $_POST["Password"];
		createUser($mysqli, $CusID, $Name, $Pass);
	} else {
		$CusID = 0;
		$Pass = "";
	}
}

function getNextID($conn) {
	$sql = "SELECT MAX(id) AS next FROM customer";
	$result = $conn->query($sql);
	$row = $result->fetch_assoc();
	return $row['next'] + 1;
}

function createUser($conn, $CustomerID, $CustomerName, $Password) {
	$salt = generateSalt();
	$hash = hash("sha256",$salt.$Password);
	
	if(!$conn->connect_error) {
		$startingCoins = 100;
		$startingSpins = 0;
		
		$sql = $conn->prepare("INSERT INTO customer VALUES (?,?,?,?,?,?)");
		$sql->bind_param('isiiss', $CustomerID, $CustomerName, $startingCoins, $startingSpins, $salt, $hash);
		$sql->execute();
		
		$results = $sql->get_result();
	}
}

//Inelegant quick way of creating a salt.
function generateSalt($max = 12) {
        $characterList = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%&*?";
        $i = 0;
        $salt = "";
        while ($i < $max) {
            $salt .= $characterList{mt_rand(0, (strlen($characterList) - 1))};
            $i++;
        }
        return $salt;
}
?>

<form action="SpinResults.php" method="post"?>
 <p>Your ID: <input type="text" name="ID" value="<?php echo $CusID; ?>" /></p>
 <p>Password: <input type="text" name="Password" value="<?php echo $Pass; ?>" /></p>
 <p>Coins Bet: <input type="text" name="CoinsBet" value= "0" /></p>
 <p>Coins Won: <input type="text" name="CoinsWon" value= "0" /></p>
 <p><input type="submit" value="Spin!"/></p>
</form>